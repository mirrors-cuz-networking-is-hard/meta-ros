# Copyright (c) 2020 LG Electronics, Inc.

# ERROR: librealsense2-2.38.1-4-r0 do_package_qa: QA Issue:
# non -dev/-dbg/nativesdk- package contains symlink .so: librealsense2 path '/work/core2-64-oe-linux/librealsense2/2.38.1-4-r0/packages-split/librealsense2/usr/lib/librealsense2.so' [dev-so]
inherit ros_insane_dev_so
